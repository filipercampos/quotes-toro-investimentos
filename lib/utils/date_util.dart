import 'package:intl/intl.dart';

class DateUtil {
  static DateTime toDate(String dateString) {
    if (dateString.isEmpty || dateString == null) {
      return null;
    }
    return DateFormat("dd/MM/yyyy").parse(dateString);
  }

  /// Formata a data em formato dd/MM/yyyy
  static String toFormat(DateTime date) {
    var formatter = DateFormat('dd/MM/yyyy');
    String formatted = formatter.format(date);
    return formatted;
  }

  /// Formata a data em formato dd/MM/yyyy
  static String toFormatFull(DateTime date) {
    var formatter = DateFormat('dd/MM/yyyy HH:mm:ss');
    String formatted = formatter.format(date);
    return formatted;
  }



  ///Converte o int em DateTime
  static DateTime toDateFromMillisecondsSinceEpoch(int millisecondsSinceEpoch) {
    if(millisecondsSinceEpoch == null){
      return null;
    }
    return DateTime.fromMillisecondsSinceEpoch(millisecondsSinceEpoch);
  }

  static DateTime toMillisecondsSinceEpoch(double value) {

    return DateTime.fromMillisecondsSinceEpoch( value.toInt() * 1000);

  }
}