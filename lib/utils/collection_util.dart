import 'package:desafio_toro_investimento/model/quote.dart';

class CollectionUtil{

  ///Min value
  static min (List<Quote> values) {
    return values.reduce((a, b) {
      if (a.value < b.value)
        return a;
      else if (a.value > b.value)
        return b;
      else
        return a;
    });
  }

  ///Max value
  static max(List<Quote> values) {
    return values.reduce((a, b) {
      if (a.value < b.value)
        return b;
      else if (a.value > b.value)
        return a;
      else
        return a;
    });
  }
}