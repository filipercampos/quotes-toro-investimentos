import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:desafio_toro_investimento/blocs/quote_bloc.dart';
import 'package:desafio_toro_investimento/ui/home_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => QuoteBloc()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        supportedLocales: [
          const Locale('en'),
          const Locale('br'),
        ],
        title: 'Toro Investimento',

        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomeScreen(),
      )
    );
  }
}
