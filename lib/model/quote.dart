import 'dart:convert';

import 'package:desafio_toro_investimento/utils/date_util.dart';

class Quote {
  int id = 0;
  String quoteName;
  double value;
  DateTime timestamp;

  Quote({this.quoteName, this.value, this.timestamp});

  factory Quote.fromJson(json) {
    var jsonData = jsonDecode(json);
    var key = jsonData.keys.first;

    return Quote(
      quoteName: key,
      value: jsonData[key],
      timestamp: DateUtil.toMillisecondsSinceEpoch(jsonData['timestamp']),
    );
  }

  @override
  String toString() {
    return '$quoteName-${value.toString().replaceAll('.', ',')}-${DateUtil.toFormatFull(timestamp)}';
  }
}
