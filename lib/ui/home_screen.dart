import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:desafio_toro_investimento/blocs/quote_bloc.dart';
import 'package:desafio_toro_investimento/model/quote.dart';
import 'package:desafio_toro_investimento/states/app_state.dart';
import 'package:desafio_toro_investimento/ui/chart_screen.dart';
import 'package:desafio_toro_investimento/widgets/quote_tile.dart';
import 'package:flutter/material.dart';

///Home screen app
class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final QuoteBloc bloc = BlocProvider.getBloc<QuoteBloc>();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text('Quotes'),
          centerTitle: true,
          backgroundColor: Colors.black,
          bottom: TabBar(
            indicatorColor: Colors.white,
            labelColor: Colors.white,
            tabs: <Widget>[
              Tab(
                icon: Icon(
                  Icons.trending_up,
                  color: Colors.greenAccent,
                ),
                text: 'TOP +5',
              ),
              Tab(
                  icon: Icon(
                    Icons.trending_down,
                    color: Colors.red,
                  ),
                  text: 'TOP -5'),
            ],
          ),
        ),
        body: StreamBuilder<AppState>(
            stream: bloc.outSocketState,
            builder: (context, snapshot) {
              if (snapshot.data == null ||
                  !snapshot.hasData ||
                  snapshot.data == AppState.FAIL||
                  snapshot.data == AppState.SUCCESS
              ) {
                return Center(
                  child: Container(
                    alignment: Alignment.center,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          snapshot.data == AppState.FAIL
                              ? 'Websocket off !'
                              : 'Websocket done !',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Icon(
                          snapshot.data == AppState.FAIL
                              ? Icons.error
                              : Icons.done,
                          color: snapshot.data == AppState.FAIL
                              ? Colors.red
                              : Colors.green,
                          size: 36,
                        ),
                      ],
                    ),
                  ),
                );
              }

              return TabBarView(
                children: [
                  _buildTab(bloc.outQuotesHigh, 1),
                  _buildTab(bloc.outQuotesLow, 0),
                ],
              );
            }),
      ),
    );
  }

  _buildTab(stream, rank) {
    return StreamBuilder<List<Quote>>(
      stream: stream,
      builder: (context, snapshot) {
        return ListView.builder(
          itemBuilder: (context, index) {
            if (snapshot.data == null ||
                !snapshot.hasData ||
                snapshot.data.length == 0) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                    strokeWidth: 2.0,
                  ),
                ),
              );
            }

            return Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
              child: InkWell(
                onTap: () async {

                  //selected symbol
                  var data = snapshot.data[index];
                  //notify that symbol must listen
                  bloc.beginEvolution(data);

                  //call graphic screen
                   await Navigator.of(context).push(
                     MaterialPageRoute(
                       builder: (context) => ChartScreen(data),
                     ),
                  );
                   //end
                  bloc.endEvolution();
                },

                child: QuoteTile(snapshot.data[index], rank),
              ),
            );
          },
          itemCount: snapshot.data != null ? snapshot.data.length : 0,
        );
      },
    );
  }
}
