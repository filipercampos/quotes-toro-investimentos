import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:desafio_toro_investimento/blocs/quote_bloc.dart';
import 'package:desafio_toro_investimento/model/quote.dart';
import 'package:desafio_toro_investimento/utils/date_util.dart';
import 'package:flutter/material.dart';

class QuoteSelectionPointLineChartScreen extends StatefulWidget {
  final String quoteName;

  QuoteSelectionPointLineChartScreen(this.quoteName);

  // We need a Stateful widget to build the selection details with the current
  // selection as the state.
  @override
  State<StatefulWidget> createState() => _SelectionCallbackState();
}

class _SelectionCallbackState
    extends State<QuoteSelectionPointLineChartScreen> {
  DateTime _time;
  Map<String, num> _measures;
  bool _animate = true;

  _SelectionCallbackState() {
    //animation just on first time graphic appear
    Future.delayed(Duration(milliseconds: 1200)).then((_) => _animate = false);
  }

  // Listens to the underlying selection changes, and updates the information
  // relevant to building the primitive legend like information under the
  // chart.
  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;

    DateTime time;
    final measures = <String, num>{};

    // We get the model that updated with a list of [SeriesDatum] which is
    // simply a pair of series & datum.
    //
    // Walk the selection updating the measures map, storing off the sales and
    // series name for each selection point.
    if (selectedDatum.isNotEmpty) {
      time = selectedDatum.first.datum.timestamp;
      selectedDatum.forEach((charts.SeriesDatum datumPair) {
        measures[datumPair.series.displayName] = datumPair.datum.value;
      });
    }

    // Request a build.
    setState(() {
      _time = time;
      _measures = measures;
    });
  }

  @override
  Widget build(BuildContext context) {
    final QuoteBloc bloc = BlocProvider.getBloc<QuoteBloc>();

    return StreamBuilder(
      stream: bloc.outQuotesEvolution,
      builder: (context, snapshot) {
        if (snapshot.data == null ||
            !snapshot.hasData ||
            snapshot.data.length == 0) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                strokeWidth: 2.0,
              ),
            ),
          );
        }
        final List<charts.Series> seriesList = _buildData(snapshot.data);

        // The children consist of a Chart and Text widgets below to hold the info.
        final children = <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 64.0),
            child: Text(
              'Current: ${snapshot.data.last.value.toStringAsFixed(2)}',
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.blue,
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height / 2 - 20,
            child: charts.TimeSeriesChart(
              seriesList,
              animate: _animate,
              defaultRenderer: charts.LineRendererConfig(
                includePoints: true,
                includeArea: true,
                areaOpacity: 0.3,
              ),
              selectionModels: [
                charts.SelectionModelConfig(
                  type: charts.SelectionModelType.info,
                  changedListener: _onSelectionChanged,
                ),
              ],
            ),
          ),
        ];

        // If there is a selection, then include the details.
        if (_time != null) {
          children.add(
            Padding(
              padding: EdgeInsets.only(top: 5.0),
              child: Text(
                DateUtil.toFormatFull(_time),
              ),
            ),
          );
        }

        _measures?.forEach((String series, num value) {
          children.add(Text('$series: $value'));
        });

        return Container(
          padding: EdgeInsets.all(16.0),
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: children,
          ),
        );
      },
    );
  }

  /// Create one series with sample hard coded data.
  List<charts.Series<Quote, DateTime>> _buildData(data) {
    return [
      charts.Series<Quote, DateTime>(
        id: 'Quotes Higher',
        domainFn: (Quote sales, _) => sales.timestamp,
        measureFn: (Quote sales, _) => sales.value,
        data: data,
      ),
    ];
  }
}
