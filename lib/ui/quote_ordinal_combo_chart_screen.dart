import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:desafio_toro_investimento/blocs/quote_bloc.dart';
import 'package:desafio_toro_investimento/model/quote.dart';
import 'package:desafio_toro_investimento/utils/date_util.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

/// Example of an ordinal combo chart with two series rendered as bars, and a
/// third rendered as a line.

class QuoteOrdinalComboChartScreen extends StatefulWidget {
  final String quoteName;

  QuoteOrdinalComboChartScreen(this.quoteName);

  @override
  _QuoteOrdinalComboChartScreenState createState() => _QuoteOrdinalComboChartScreenState();
}

class _QuoteOrdinalComboChartScreenState extends State<QuoteOrdinalComboChartScreen> {

  bool _animate = true;

  _QuoteOrdinalComboChartScreenState(){
    //animation just on first time graphic appear
    Future.delayed(Duration(milliseconds: 1200)).then((_) => _animate = false);
  }

  @override
  Widget build(BuildContext context) {
    final QuoteBloc bloc = BlocProvider.getBloc<QuoteBloc>();

    return StreamBuilder(
      stream: bloc.outQuotesEvolution,
      builder: (context, snapshot) {
        if (snapshot.data == null ||
            !snapshot.hasData ||
            snapshot.data.length == 0) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                strokeWidth: 2.0,
              ),
            ),
          );
        }

        final List<charts.Series> seriesList = _buildData(snapshot.data);
        return Container(
          padding: EdgeInsets.all(16.0),
          color: Colors.white,
          child: new charts.OrdinalComboChart(seriesList,
              animate: _animate,
              // Configure the default renderer as a bar renderer.
              defaultRenderer: new charts.BarRendererConfig(
                groupingType: charts.BarGroupingType.grouped,
              ),
              // Custom renderer configuration for the line series. This will be used for
              // any series that does not define a rendererIdKey.
              customSeriesRenderers: [
                new charts.LineRendererConfig(
                    // ID used to link series to this renderer.
                    customRendererId: 'customLine')
              ]),
        );
      },
    );
  }

  /// Create series list with multiple series
  List<charts.Series<Quote, String>> _buildData(data) {
    return [
      new charts.Series<Quote, String>(
          id: 'Line',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (Quote quote, _) => DateUtil.toFormat(quote.timestamp),
          measureFn: (Quote quote, _) => quote.value,
          data: data),
      new charts.Series<Quote, String>(
          id: 'Bar ',
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
          domainFn: (Quote quote, _) => DateUtil.toFormat(quote.timestamp),
          measureFn: (Quote quote, _) => quote.value,
          data: data)

        // Configure our custom line renderer for this series.
        ..setAttribute(charts.rendererIdKey, 'customLine'),
    ];
  }
}
