import 'package:desafio_toro_investimento/model/quote.dart';
import 'package:desafio_toro_investimento/ui/quote_selection_point_line_chart.dart';
import 'package:desafio_toro_investimento/ui/quote_ordinal_combo_chart_screen.dart';
import 'package:flutter/material.dart';

///Home screen app
class ChartScreen extends StatefulWidget {
  final Quote quote;

  ChartScreen(this.quote);

  @override
  _ChartScreenState createState() => _ChartScreenState();
}

class _ChartScreenState extends State<ChartScreen> {
  @override
  Widget build(BuildContext context) {
    String key = widget.quote.quoteName;

    return DefaultTabController(
      length: 2,
      child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: Text(key),
            centerTitle: true,
            backgroundColor: Colors.black,
            bottom: TabBar(
              indicatorColor: Colors.white,
              labelColor: Colors.white,
              tabs: <Widget>[
                Tab(
                  icon: Icon(
                    Icons.timeline,
                    color: Colors.white,
                  ),
                  text: 'Line',
                ),
                Tab(
                  icon: Icon(
                    Icons.insert_chart,
                    color: Colors.white,
                  ),
                  text: 'Bar',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              QuoteSelectionPointLineChartScreen(key),
              QuoteOrdinalComboChartScreen(key),
              //QuoteScreenPointLine(key),
            ],
          )),
    );
  }
}
