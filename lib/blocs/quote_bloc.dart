import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:desafio_toro_investimento/config/app_values.dart';
import 'package:desafio_toro_investimento/model/quote.dart';
import 'package:desafio_toro_investimento/states/app_state.dart';
import 'package:desafio_toro_investimento/states/sort_criteria.dart';
import 'package:rxdart/rxdart.dart';
import 'package:web_socket_channel/io.dart';
import 'dart:io' show Platform;

///Business Logic Component
///
/// Separação da logica de negócio da sua aplicação e da UI
/// através de Streams: uma fonte de eventos assíncronos.
class QuoteBloc extends BlocBase {
  //IP for Android not is local host
  final channel = IOWebSocketChannel.connect(Platform.isAndroid
      ? AppValues.QUOTES_URL_ANDROID
      : AppValues.QUOTES_URL_IOS);

  final _quotesHighController = BehaviorSubject<List<Quote>>();
  final _quotesLowController = BehaviorSubject<List<Quote>>();
  final _socketStateController = BehaviorSubject<AppState>();
  final _quotesEvolutionController = BehaviorSubject<List<Quote>>();

  //all quotes in memory
  final _quotesDict = Map<String, List<Quote>>();

  String _symbol;

  Stream<List<Quote>> get outQuotesHigh => _quotesHighController.stream;

  Stream<List<Quote>> get outQuotesLow => _quotesLowController.stream;

  Stream get quoteStream => channel.stream;

  Stream<AppState> get outSocketState => _socketStateController.stream;

  Stream<List<Quote>> get outQuotesEvolution => _quotesEvolutionController.asBroadcastStream();

  StreamSubscription _subscription;

  QuoteBloc() {
    _socketStateController.add(AppState.IDLE);
    _quotesLowController.value = List<Quote>();
    _quotesHighController.value = List<Quote>();
    _quotesEvolutionController.value = List<Quote>();

    // update realtime
    _subscription = channel.stream.listen((data) {
      Quote quote = Quote.fromJson(data);

      if (quote != null) {
        //print('$quote'); //debug

        //Top 5+
        _top5Plus(quote);

        //Top 5-
        _top5Minus(quote);

        //Top 10 evolution all quotes
        _evolution(quote);
      }
    }, onError: (error) {
      this._socketStateController.add(AppState.FAIL);

      print(error);
      if (_subscription != null) {
        _subscription.cancel();
      }
    }, onDone: () {
      this._socketStateController.add(AppState.SUCCESS);
    });
  }

  ///Real time higher quote TOP +5
  _top5Plus(quote) {
    var quotes = _quotesHighController.value;

    if (quotes.isEmpty) {
      //add list
      _quotesHighController.value.add(quote);
    } else {
      //o symbol exists
      if (_checkSymbol(_quotesHighController, quote)) {
        //update
        _swapMaxExists(quote);
      } else if (quotes.length < 5) {
        //add at list
        _quotesHighController.value.add(quote);
      } else {
        //swap / pop / push
        _swapMaxPopPush(quote);
      }
    }

    _sortCriteria(_quotesHighController, SortCriteria.HIGH);
  }

  ///Ensures that there is always a higher value symbol
  ///that does not repeat day after day
  _swapMaxExists(Quote quote) {
    for (int i = 0; i < _quotesHighController.value.length; i++) {
      Quote q = _quotesHighController.value[i];

      //exists same day
      if (quote.quoteName == q.quoteName &&
          quote.value > q.value &&
          quote.timestamp.compareTo(q.timestamp) > 0) {
        q.value = quote.value;

        i = _quotesHighController.value.length;
      } else {
        //now it get in
        //it not exists yet at day
        //maybe value is down
        if (quote.quoteName == q.quoteName &&
            quote.timestamp.compareTo(q.timestamp) == 1) {
          q.value = quote.value;
          q.timestamp = quote.timestamp;
          i = _quotesHighController.value.length;
        }
      }
    }
  }

  ///Ensures there is always a higher value symbol
  ///even if there is no
  _swapMaxPopPush(Quote quote) {
    for (int i = 0; i < _quotesHighController.value.length; i++) {
      Quote q = _quotesHighController.value[i];

      if (quote.value > q.value && quote.timestamp.compareTo(q.timestamp) > 0) {
        //remove element
        this._quotesHighController.value.remove(q);
        this._quotesHighController.value.add(quote);

        i = _quotesHighController.value.length;
      }
    }
  }

  ///Real time higher quote TOP -5
  _top5Minus(quote) {
    var quotes = _quotesLowController.value;

    if (quotes.isEmpty) {
      //add list
      _quotesLowController.value.add(quote);
    } else {
      //o symbol exists
      if (_checkSymbol(_quotesLowController, quote)) {
        //update
        _swapMinExists(quote);
      } else if (quotes.length < 5) {
        //add at list
        _quotesLowController.value.add(quote);
      } else {
        //swap / pop / push
        _swapMinPopPush(quote);
      }
    }

    _sortCriteria(_quotesLowController, SortCriteria.LOW);
  }

  ///Ensures that there is always a higher value symbol
  ///that does not repeat day after day
  _swapMinExists(Quote quote) {
    for (int i = 0; i < _quotesLowController.value.length; i++) {
      Quote q = _quotesLowController.value[i];

      if (quote.quoteName == q.quoteName &&
          quote.value < q.value &&
          quote.timestamp.compareTo(q.timestamp) > 0) {
        q.value = quote.value;
        i = _quotesLowController.value.length;
      } else {
        //now it get in
        //it not exists yet at day
        //maybe value is down
        if (quote.quoteName == q.quoteName &&
            quote.timestamp.compareTo(q.timestamp) == 1) {
          q.value = quote.value;
          q.timestamp = quote.timestamp;
          i = _quotesLowController.value.length;
        }
      }
    }
  }

  ///Ensures there is always a higher value symbol
  ///even if there is no
  _swapMinPopPush(Quote quote) {
    for (int i = 0; i < _quotesLowController.value.length; i++) {
      Quote q = _quotesLowController.value[i];

      if (quote.value < q.value && quote.timestamp.compareTo(q.timestamp) > 0) {
        //remove element
        _quotesLowController.value.remove(q);
        _quotesLowController.value.add(quote);

        i = _quotesLowController.value.length;
      }
    }
  }

  ///Check symbol exists
  _checkSymbol(controller, Quote quote) {
    for (int i = 0; i < controller.value.length; i++) {
      Quote q = controller.value[i];

      if (q.quoteName == quote.quoteName) {
        return true;
      }
    }
    return false;
  }

  ///Sort quotes HIGH or LOW
  void _sortCriteria(controller, SortCriteria criteria) {
    //compare value
    switch (criteria) {
      //low to high
      case SortCriteria.HIGH:
        controller.value.sort((a, b) {
          if (a.value < b.value)
            return 1;
          else if (a.value > b.value)
            return -1;
          else
            return 0;
        });

        break;
      //high to low
      case SortCriteria.LOW:
        controller.value.sort((a, b) {
          if (a.value > b.value)
            return 1;
          else if (a.value < b.value)
            return -1;
          else
            return 0;
        });

        break;
    }
    controller.add(controller.value);
  }

  ///Evolution 10 last quotes all symbol
  ///Stack all quotes but listen one only SYMBOL
  void _evolution(Quote quote) async {
    var values = _quotesDict;

    var key = quote.quoteName;
    if (values.isNotEmpty && values.containsKey(quote.quoteName)) {
      //quotes from key
      var quotes = values[key];

      //keep list updated
      quotes.add(quote);

      //build
      //1,2,3,4,5,6,7,8,9,10,11

      //remove first
      if (quotes.length > 10) {
        quotes.removeAt(0);
      }

      //do
      //2,3,4,5,6,7,8,9,10,11

      //save quotes in memory
      _quotesDict[quote.quoteName] = quotes;

      //listen only graphic selected
      if (key == _symbol|| quotes.length < 10 && key == _symbol ) {
        //graphic
        _quotesEvolutionController.sink.add(quotes);
      }
    } else {
      //create key
      var quotes = List<Quote>();
      quotes.add(quote);
      _quotesDict[quote.quoteName] = quotes;
    }
  }

  ///Set graphic to listen
  void beginEvolution(Quote q) {
    this._symbol = q.quoteName;
  }

  ///Ensure that each graphic has different values
  void endEvolution() {
    this._symbol = null;
    //this._quotesEvolutionController.value.clear();
  }

  @override
  void dispose() {
    super.dispose();
    if (channel != null) {
      channel.sink.close();
    }
    _quotesLowController.close();
    _quotesHighController.close();
    _subscription.cancel();
    _socketStateController.close();
    _quotesEvolutionController.close();
  }
}
