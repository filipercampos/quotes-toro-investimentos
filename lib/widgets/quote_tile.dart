import 'package:desafio_toro_investimento/model/quote.dart';
import 'package:desafio_toro_investimento/utils/date_util.dart';
import 'package:flutter/material.dart';

class QuoteTile extends StatelessWidget {
  final Quote quote;
  final int rank;

  QuoteTile(this.quote, this.rank);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: Colors.white, width: 2),
        ),
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 12.0, bottom: 8.0),
              child: Text(
                this.quote.quoteName,
                style: TextStyle(fontSize: 32, color: Colors.white),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 5.0),
              child: Text(
                this.quote.value.toStringAsFixed(2),
                style: TextStyle(
                  fontSize: 16,
                  color: rank == 1 ? Colors.greenAccent : Colors.red,
                ),
              ),
            ),
            Container(
              child: Text(
                DateUtil.toFormat(this.quote.timestamp),
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
              ),
              margin: EdgeInsets.only(bottom: 5.0),
            ),
          ],
        ),
      ),
    );
  }
}
